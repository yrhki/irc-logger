

build:
	go build -o irc-logger ./cmd

build.clean:
	go build -trimpath -o irc-logger -ldflags "-s -w" ./cmd
