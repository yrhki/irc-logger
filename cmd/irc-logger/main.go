package main

import (
	"flag"
	"fmt"
	"git.raspi.lan/yrski/irc-logger/logger"
	"log"
	"os"
	"os/signal"
	"strings"
)

var (
    flagStart   int64
    flagTLS     bool
    channel     []string
    server      string
    debug       *log.Logger
    nickname, password, username  string
    chanInterrupt chan os.Signal = make(chan os.Signal, 1)
)

func parseFlags() error {
    server = flag.Arg(0)
    if server == "" {
        return fmt.Errorf("No server")
    }
    channel = strings.Split(flag.Arg(1), ",")
    if len(channel) == 0 {
        return fmt.Errorf("No channel")
    }

    nickname = os.Getenv("NICK")
    if nickname == "" {
        return fmt.Errorf("No nick")
    }
    password = os.Getenv("PASS")
    if password == "" {
        return fmt.Errorf("No pass")
    }

    return nil
}

func main() {
    flag.Int64Var(&flagStart, "starttime", 0, "Stream start in unix seconds")
    flag.BoolVar(&flagTLS, "tls", false, "Use TLS")
    flag.Parse()

    err := parseFlags()
    if err != nil {
        fmt.Fprintln(os.Stdout, err)
        os.Exit(1)
    }
    debug = log.New(os.Stderr, "irc-logger ", log.LstdFlags | log.LUTC)

    options := logger.IRCLoggerOptions{
        TLS: flagTLS,
        Timestamp: flagStart,
        Server: server,
        Channels: channel,
        Nickname: nickname,
        Password: password,
        Debug: debug,
        Output: os.Stdout,
    }

    logc, err := logger.NewIRCLogger(&options)
    if err != nil {
        panic(err)
    }

    signal.Notify(chanInterrupt, os.Interrupt)
    go func() {
        <-chanInterrupt
        debug.Println("Stopping")
        logc.Stop()
    }()

    if err = logc.Run(); err != nil {
        panic(err)
    }
}

