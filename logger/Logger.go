package logger

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"time"

	"gopkg.in/irc.v3"
)



type IRCLogger struct {
    client   *irc.Client
    options  *IRCLoggerOptions
    start    time.Time
    runStart time.Time
}


type IRCLoggerOptions struct {
    TLS       bool
    Timestamp int64
    Server    string
    Channels  []string
    Nickname, Password, Username string
    Output    io.Writer
    Debug     *log.Logger
}

func NewIRCLogger(options *IRCLoggerOptions) (*IRCLogger, error) {
    var (
        con io.ReadWriteCloser
        err error
        logger IRCLogger = IRCLogger{options:options}
    )

    if options.Output == nil {
        return nil, errors.New("Output is nil")
    }

    if options.TLS {
        tlsConfig := tls.Config{}
        con, err = tls.Dial("tcp", options.Server, &tlsConfig)
    } else {
        con, err = net.Dial("tcp", options.Server)
    }
    if err != nil { return nil, err }
    config := irc.ClientConfig{
        Nick: options.Nickname,
        Pass: options.Password,
        Handler: irc.HandlerFunc(logger.handler),
    }
    logger.client = irc.NewClient(con, config)
    if options.Timestamp != 0 {
        logger.start = time.Unix(options.Timestamp, 0).UTC()
    } else {
        logger.start = time.Now().UTC()
    }
    logger.runStart = time.Now().UTC()
    return &logger, nil
}

func (logger *IRCLogger) Run() error {
    err := logger.client.Run()
    if err != io.EOF {
        return err
    }
    return nil
}

func (logger *IRCLogger) Stop() error {
    err := logger.client.Write("QUIT")
    if logger.options.Timestamp != 0 {
        d := logger.RunDuration()
        fmt.Fprintf(logger.options.Output, "END: %s, %v\n", time.Now().UTC(), d)
    }
    return err
}

func (logger *IRCLogger) debug(v ...interface{}) {
    if logger.options.Debug != nil {
        logger.options.Debug.Println(v...)
    }
}

func (logger *IRCLogger) Duration() time.Duration {
    if logger.options.Timestamp != 0 {
        return time.Now().UTC().Sub(logger.start)
    }
    return 0
}

func (logger *IRCLogger) RunDuration() time.Duration {
    return time.Now().UTC().Sub(logger.runStart)
}

func (logger *IRCLogger) handler(c *irc.Client, m *irc.Message) {
    connected := false
    if connected && m.Command != "PRIVMSG" {
        logger.debug(m)
    }
    switch m.Command {
    case irc.RPL_MOTD:
        logger.debug(m.Trailing())
    case irc.RPL_WELCOME:
        fmt.Fprintf(logger.options.Output, "START: %s\n", time.Now().UTC())

        for _, v := range logger.options.Channels {
            c.Write("JOIN " + v)
            logger.debug("Join: " + v)
        }
        logger.debug("Connected")
        connected = true
    default:
        if c.FromChannel(m) && m.Command == "PRIVMSG" {
            t := time.Now().UTC()
            if logger.options.Timestamp != 0 {
                d := t.Sub(logger.start)
                fmt.Fprintf(logger.options.Output, "[%02d:%02d:%02d] ", int64(d.Hours()), int64(d.Minutes()) % 60, int64(d.Seconds()) % 60)
            } else {
                fmt.Fprintf(logger.options.Output, "[%s] ", t.Format(time.RFC3339))
            }
            if len(logger.options.Channels) > 1 {
                fmt.Fprintf(logger.options.Output, "<%s%s> %s\n", m.User, m.Params[0], m.Trailing())
            } else {
                fmt.Fprintf(logger.options.Output, "<%s> %s\n", m.User, m.Trailing())
            }
        }
    }
}
